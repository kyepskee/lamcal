open Core
open Mpc

open Lamcal

let _ =
  let p = Parser.expr in
  (* let source = In_channel.(input_all stdin) in *)
  let source = In_channel.read_all "../test.lam" in
  let res = run_parser p source in
  let parsed = fst @@ Option.value_exn (LazyList.head res) in
  let print_red l =
    print_endline "";
    print_endline (Lambda.rep l);
    (* print_string "===";
     * print_endline (Lambda.show l); *)
  in
  print_endline (Lambda.rep parsed);
  if Lambda.bound parsed
  then begin
    List.iter (Lambda.reduce_steps parsed) ~f:print_red
  end
  else print_endline "Wrongly bound"
