open Core

type t = App of t * t
       | Lambda of string * t
       | Bind of string * t * t (* bind string to expression in another expression *)
       | Var of string
[@@deriving show, eq]

module Env = Map.Make (String)

let rec rep lam =
  let wrap_rep lam =
    match lam with
    | App _ | Lambda _ | Bind _ -> "("^rep lam^")"
    | Var _ -> rep lam
  in
  let wrap_rep2 lam =
    match lam with
    | Lambda _ | Bind _ -> "("^rep lam^")"
    | Var _ | App _ -> rep lam
  in
  match lam with
  | App (l1, l2) ->
    (wrap_rep2 l1) ^ " " ^ (wrap_rep l2)
  | Lambda (arg, exp) ->
    "λ"^arg^"."^(rep exp)
  | Bind (a, b, x) -> (Format.sprintf "[%s := %s]\n" a (rep b))^ rep x
  | Var str -> str

(* replaces variable until re-definition *)
let rec replace_var lam var expr =
  match lam with
  | Lambda (v, e) ->
    if String.(v = var)
    then lam (* if the variable name reappears, it means variable overshadowing - we make no changes*)
    else Lambda (v, replace_var e var expr)
  | App (e1, e2) ->
    App (replace_var e1 var expr,
         replace_var e2 var expr)
  | Bind (n, b, e) ->
    if String.(n = var)
    then lam (* again, variable overshadowing *)
    else Bind (n, replace_var b var expr, replace_var e var expr)
  | Var (v) ->
    if String.(v = var)
    then expr
    else lam

(* see if every variable is bound to something *)
let rec bound_env lam env =
  match lam with
  | Var (v) -> Set.mem env v
  | Bind (v, e1, e2) ->
    bound_env e1 env && bound_env e2 (Set.add env v)
  | Lambda (v, e) -> bound_env e (Set.add env v)
  | App (e1, e2) ->
    bound_env e1 env &&
    bound_env e2 env

let bound lam =
  bound_env lam (Set.empty (module String))

let reduce_step ?env:(env=Env.empty) lam =
  let rec try_reduce_step env lam =
    match lam with
    | App (Lambda (v, e1), e2) ->
      replace_var e1 v e2
    | App (e1, e2) ->
      let re1 = try_reduce_step env e1 in
      if equal e1 re1
      then App (e1, try_reduce_step env e2)
      else App (re1, e2)
    | Bind (v, e1, e2) ->
      replace_var e2 v e1
    | Var v ->
      begin match Env.find env v with
        | Some x -> x
        | None -> Var v
      end
    | Lambda (v, e) ->
      Lambda (v, try_reduce_step env e)
  in
  let res = try_reduce_step env lam in
  if equal res lam
  then None
  else Some res

let rec main_exp ?env:(env=Env.empty) lam =
  match lam with
  | Bind (v, e1, e2) ->
    main_exp e2 ~env:(Env.set env ~key:v ~data:e1)
  | Lambda _ | Var _ | App _ -> (lam, env)

let reduce_steps lam =
  let (lam, env) = main_exp lam in
  let rec reduce_with_env lam env =
    match reduce_step ~env lam with
    | Some l ->
      l::(reduce_with_env l env)
    | None -> []
  in
  lam :: (reduce_with_env lam env)
