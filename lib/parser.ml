open Core
open Mpc

open Lambda

let token p =  whitespace *> p
let symbol str = token @@ exactly str

let id = String.of_char_list <$> token @@ many1 lower
let var = id >>| (fun x -> Var x)

let chainl1 p op =
  let rec fold acc =
    (let%bind f = op in
     let%bind x = p in
     let%bind n = return @@ f acc x in
     fold n) <|> return acc in
  p >>= fun init -> fold init

let expr =
  let lam x y = Lambda (x, y) in
  let app x y = App (x, y) in
  let b s x y = Bind (s, x, y) in
  fix (fun expr ->
      let lambda = lam <$> symbol "\\" *> id <* symbol "." <*> expr in
      let bind = b
                 <$> symbol "[" *> id <* symbol ":="
                 <*> expr <* symbol "]"
                 <*> expr
      in
      let op = bind <|> (symbol "(" *> expr <* symbol ")") <|> var <|> lambda in
      chainl1 op (return app))
