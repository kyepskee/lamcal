open Core

type t = App of t * t
       | Lambda of string * t
       | Bind of string * t * t
       | Var of string
[@@deriving show, eq]

module Env: Map.S

val bound : t -> bool
val rep : t -> string

val replace_var : t -> string -> t -> t

val main_exp : ?env:t Env.t -> t -> t * t Env.t

val reduce_steps : t -> t list
